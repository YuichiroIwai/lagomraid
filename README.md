lagomraid
=========

フィールド上に発生する敵をプレイヤーが討伐するゲームをLagomでシミュレーションするプロジェクト。

パフォーマンス計測、およびLagom自体の学習が主たる目的。

## Usage

- install sbt (build tool)  
http://www.scala-sbt.org/index.html

- clone project  
`$ git clone https://bitbucket.org/YuichiroIwai/lagomraid.git`

- run all services (dev mode)  
`$ sbt runAll`

## Services

lagomraidを構成するService

### Player

- Player情報を管理する
- 複数のNodeにシャーディングすることでスケールする

### Player-Scheduler

- Playerの非同期タスクをスケジュールするサービス

### Bot

- Playerを自動操作する

### Boss

- Boss(敵)情報を管理する
- 複数のNodeにシャーディングすることでスケールする

### Field

- Field上のオブジェクト（PlayerおよびBoss）の位置を管理する
- 必要に応じてエリア毎の分割をおこないスケールさせる

### Handler

- システム全体の操作をおこなう
- Playerの追加、Bossの発生、統計など

