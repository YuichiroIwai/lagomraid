package com.yuiwai.lagomraid.boss.api

import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import play.api.libs.json.{Format, Json}

trait BossService extends Service {
  def spawn: ServiceCall[SpawnRequest, String]
  override def descriptor = {
    import Service._

    named("boss")
      .withCalls(
        pathCall("/api/boss", spawn _)
      )
      .withAutoAcl(true)
  }
}

case class SpawnRequest(hp: Long)
object SpawnRequest {
  implicit val format: Format[SpawnRequest] = Json.format
}
