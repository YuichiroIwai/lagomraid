package com.yuiwai.lagomraid.boss

import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.server.{LagomApplication, LagomApplicationContext, LagomApplicationLoader}
import com.softwaremill.macwire._
import com.yuiwai.lagomraid.boss.api.BossService
import play.api.libs.ws.ahc.AhcWSComponents

abstract class BossApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents {
  override def lagomServer = serverFor[BossService](wire[BossServiceImpl])
}

class BossApplicationLoader extends LagomApplicationLoader {
  override def load(context: LagomApplicationContext) = new BossApplication(context) {
    override def serviceLocator = NoServiceLocator
  }
  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new BossApplication(context) with LagomDevModeComponents
}
