package com.yuiwai.lagomraid.bot.api

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}

trait BotService extends Service {
  def ping: ServiceCall[NotUsed, Done]
  override def descriptor: Descriptor = {
    import Service._
    named("bot")
      .withCalls(
        pathCall("/api/bot/ping", ping _)
      )
      .withAutoAcl(true)
  }
}
