package com.yuiwai.lagomraid.bot

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.yuiwai.lagomraid.bot.api.BotService

import scala.concurrent.Future

class BotServiceImpl extends BotService {
  override def ping: ServiceCall[NotUsed, Done] = ServiceCall { _ =>
    Future.successful(Done)
  }
}
