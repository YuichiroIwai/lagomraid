organization in ThisBuild := "com.yuiwai"
version in ThisBuild := "0.1.0-SNAPSHOT"
scalaVersion in ThisBuild := "2.12.4"

lagomCassandraEnabled in ThisBuild := false
lagomKafkaEnabled in ThisBuild := true

val macwire = "com.softwaremill.macwire" %% "macros" % "2.2.5" % "provided"
val scalaTest = "org.scalatest" %% "scalatest" % "3.0.1" % "test"
val playJsonDerivedCodecs = "org.julienrf" %% "play-json-derived-codecs" % "4.0.0"
val mysqlConnector = "mysql" % "mysql-connector-java" % "5.1.44"
val h2 = "com.h2database" % "h2" % "1.4.196"

lazy val root = (project in file("."))
  .settings(
    name := "lagomraid"
  )
  .aggregate(
    `player-api`, `player-impl`,
    `player-scheduler-api`, `player-scheduler-impl`,
    `boss-api`, `boss-impl`,
    `handler-api`, `handler-impl`,
    `field-api`, `field-impl`
  )

lazy val core = (project in file("core"))
  .settings(
    name := "lagomraid-core",
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      playJsonDerivedCodecs
    )
  )

lazy val `player-api` = (project in file("player-api"))
  .settings(
    name := "lagomraid-player-api"
  )
  .dependsOn(core)
lazy val `player-impl` = (project in file("player-impl"))
  .enablePlugins(LagomScala)
  .settings(lagomForkedTestSettings: _*)
  .settings(
    name := "lagomraid-player-impl",
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceJdbc,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      macwire,
      scalaTest,
      playJsonDerivedCodecs,
      mysqlConnector,
      h2
    )
  )
  .dependsOn(`player-api`, `field-api`, `player-scheduler-api`)

lazy val `player-scheduler-api` = (project in file("player-scheduler-api"))
  .settings(
    name := "lagomraid-player-scheduler-api"
  )
  .dependsOn(core)
lazy val `player-scheduler-impl` = (project in file("player-scheduler-impl"))
  .enablePlugins(LagomScala)
  .settings(lagomForkedTestSettings: _*)
  .settings(
    name := "lagomraid-player-scheduler-impl",
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceJdbc,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      macwire,
      scalaTest,
      playJsonDerivedCodecs,
      mysqlConnector,
      h2
    )
  )
  .dependsOn(`player-api`, `player-scheduler-api`)


lazy val `boss-api` = (project in file("boss-api"))
  .settings(
    name := "lagomraid-boss-api"
  )
  .dependsOn(core)
lazy val `boss-impl` = (project in file("boss-impl"))
  .enablePlugins(LagomScala)
  .settings(lagomForkedTestSettings: _*)
  .settings(
    name := "lagomraid-boss-impl",
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceJdbc,
      lagomScaladslTestKit,
      macwire,
      scalaTest,
      playJsonDerivedCodecs,
      mysqlConnector,
      h2
    )
  )
  .dependsOn(`boss-api`)

lazy val `field-api` = (project in file("field-api"))
  .settings(
    name := "lagomraid-field-api"
  )
  .dependsOn(core)
lazy val `field-impl` = (project in file("field-impl"))
  .enablePlugins(LagomScala)
  .settings(lagomForkedTestSettings: _*)
  .settings(
    name := "lagomraid-field-impl",
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceJdbc,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      macwire,
      scalaTest,
      playJsonDerivedCodecs,
      mysqlConnector,
      h2
    )
  )
  .dependsOn(`field-api`, `player-api`)

lazy val `handler-api` = (project in file("handler-api"))
  .settings(
    name := "lagomraid-handler-api"
  )
  .dependsOn(core)
lazy val `handler-impl` = (project in file("handler-impl"))
  .enablePlugins(LagomScala)
  .settings(lagomForkedTestSettings: _*)
  .settings(
    name := "lagomraid-handler-impl",
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceJdbc,
      macwire,
      scalaTest,
      mysqlConnector,
      h2
    )
  )
  .dependsOn(`handler-api`, `player-api`, `field-api`)


lazy val `bot-api` = (project in file("bot-api"))
  .settings(
    name := "bot-api",
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )
lazy val `bot-impl` = (project in file("bot-impl"))
  .enablePlugins(LagomScala)
  .settings(
    name := "bot-impl",
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceJdbc,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      macwire,
      scalaTest,
      mysqlConnector,
      h2
    )
  )
  .settings(lagomForkedTestSettings: _*)
  .dependsOn(`bot-api`)
