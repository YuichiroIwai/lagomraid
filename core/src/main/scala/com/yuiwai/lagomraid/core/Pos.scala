package com.yuiwai.lagomraid.core

import play.api.libs.json.{Format, Json}

case class Pos(x: Int, y: Int)
object Pos {
  implicit val format: Format[Pos] = Json.format
  def random(max: Int) = Pos((Math.random() * max).toInt, (Math.random() * max).toInt)
}

sealed trait FieldObject {
  val pos: Pos
}
case class PlayerObject(pos: Pos, playerId: String) extends FieldObject
case class BossObject(pos: Pos, bossId: String) extends FieldObject
