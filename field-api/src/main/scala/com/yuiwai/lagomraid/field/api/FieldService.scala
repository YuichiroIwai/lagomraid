package com.yuiwai.lagomraid.field.api

import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import com.yuiwai.lagomraid.core.Pos
import play.api.libs.json._
import julienrf.json.derived

object FieldService {
  val PLAYER_TOPIC_NAME = "fieldPlayerTopic"
}
trait FieldService extends Service {
  val MAX_POS = 512
  def findObjects: ServiceCall[FindObjectsRequest, Seq[FieldObject]]
  def putPlayer: ServiceCall[PutPlayerRequest, Pos]
  def playerTopic: Topic[PlayerMessage]
  override def descriptor = {
    import Service._

    named("field")
      .withCalls(
        pathCall("/api/field/objects", findObjects _),
        pathCall("/api/field/player", putPlayer _)
      )
      .withTopics(
        topic(FieldService.PLAYER_TOPIC_NAME, playerTopic)
      )
      .withAutoAcl(true)
  }
}

case class FindObjectsRequest(pos: Pos)
object FindObjectsRequest {
  implicit val format: Format[FindObjectsRequest] = Json.format
}

case class FieldObject(pos: Pos, fieldObjectType: Int, entityId: String)
object FieldObject {
  implicit val format: Format[FieldObject] = Json.format
}

case class PutPlayerRequest(playerId: String)
object PutPlayerRequest {
  implicit val format: Format[PutPlayerRequest] = Json.format
}

sealed trait PlayerMessage {
  val playerId: String
}
case class PlayerPutMessage(playerId: String, pos: Pos) extends PlayerMessage
object PlayerPutMessage {
  implicit val format: Format[PlayerPutMessage] = Json.format
}
object PlayerMessage {
  implicit val format: Format[PlayerMessage] =
    derived.flat.oformat((__ \ "type").format[String])
}

object FieldObjectTypes {
  val Player = 1
  val Boss = 2
}
