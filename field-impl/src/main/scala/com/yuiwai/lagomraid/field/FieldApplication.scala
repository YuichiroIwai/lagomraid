package com.yuiwai.lagomraid.field

import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaComponents
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.persistence.slick.SlickPersistenceComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.lightbend.lagom.scaladsl.server.{LagomApplication, LagomApplicationContext, LagomApplicationLoader}
import com.softwaremill.macwire._
import com.yuiwai.lagomraid.field.api.FieldService
import com.yuiwai.lagomraid.player.api.PlayerService
import play.api.db.HikariCPComponents
import play.api.libs.ws.ahc.AhcWSComponents

import scala.collection.immutable

abstract class FieldApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with LagomKafkaComponents
    with SlickPersistenceComponents
    with HikariCPComponents
    with AhcWSComponents {
  override def lagomServer = serverFor[FieldService](wire[FieldServiceImpl])
  override def jsonSerializerRegistry: JsonSerializerRegistry = FieldSerializerRegistry
  val playerService = serviceClient.implement[PlayerService]
  persistentEntityRegistry.register(wire[FieldEntity])
  wire[PlayerSubscriber]
}

class FieldApplicationLoader extends LagomApplicationLoader {
  override def load(context: LagomApplicationContext) = new FieldApplication(context) {
    override def serviceLocator = NoServiceLocator
  }
  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new FieldApplication(context) with LagomDevModeComponents
}

object FieldSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: immutable.Seq[JsonSerializer[_]] = immutable.Seq()
}
