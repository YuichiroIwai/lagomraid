package com.yuiwai.lagomraid.field

import com.lightbend.lagom.scaladsl.persistence.{AggregateEvent, AggregateEventTag, PersistentEntity}
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity.ReplyType
import com.yuiwai.lagomraid.core.{FieldObject, PlayerObject, Pos}

import scala.util.{Failure, Success, Try}

class FieldEntity extends PersistentEntity {
  override type Command = FieldCommand[_]
  override type Event = FieldEvent
  override type State = FieldState
  override def initialState: FieldState = FieldState(Map.empty, Map.empty)
  override def behavior: Behavior = { _ =>
    Actions()
      .onCommand[PutPlayerCommand, Try[Pos]] {
      case (PutPlayerCommand(playerId, maxPos), ctx, state) =>
        state.allocatePlayer(playerId, maxPos) match {
          case s@Success(pos) =>
            ctx.thenPersist(PlayerPut(playerId, pos))(_ => ctx.reply(s))
          case e: Failure[_] =>
            ctx.reply(e)
            ctx.done
        }
    }.onReadOnlyCommand[FindObjectsCommand, Seq[FieldObject]] {
      case(FindObjectsCommand(pos), ctx, state) =>
        // TODO 指定位置付近のフィールドオブジェクト取得
        ctx.reply(Seq.empty)
    }.onEvent {
      case (PlayerPut(playerId, pos), state) =>
        state.putPlayer(pos, playerId)
    }
  }
}

sealed trait FieldCommand[R] extends ReplyType[R]
case class FindObjectsCommand(pos: Pos) extends FieldCommand[Seq[FieldObject]]
case class PutPlayerCommand(playerId: String, maxPos: Int) extends FieldCommand[Try[Pos]]

sealed trait FieldEvent extends AggregateEvent[FieldEvent] {
  override def aggregateTag = FieldEvent.Tag
}
object FieldEvent {
  val Tag = AggregateEventTag.sharded[FieldEvent](16)
}
sealed trait FieldPlayerEvent extends FieldEvent {
  val playerId: String
}
case class PlayerPut(playerId: String, pos: Pos) extends FieldPlayerEvent

final case class FieldState(fieldData: Map[Pos, FieldObject], playerMap: Map[String, Pos]) {
  def putPlayer(pos: Pos, playerId: String): FieldState =
    put(pos, PlayerObject(pos, playerId)).copy(playerMap = playerMap.updated(playerId, pos))
  def put(pos: Pos, fieldObject: FieldObject): FieldState = copy(fieldData = fieldData.updated(pos, fieldObject))
  def allocatePlayer(playerId: String, maxPos: Int): Try[Pos] = {
    playerMap.get(playerId) match {
      case Some(pos) => Success(pos)
      case None => allocate(maxPos)
    }
  }
  def allocate(maxPos: Int, retry: Int = 100): Try[Pos] = {
    require(retry > 0, "オブジェクト配置の最大試行回数に達した")
    val pos = Pos.random(maxPos)
    if (fieldData.contains(pos)) allocate(retry - 1)
    else Success(pos)
  }
}
