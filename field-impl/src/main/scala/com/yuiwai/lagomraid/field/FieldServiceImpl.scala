package com.yuiwai.lagomraid.field

import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.broker.TopicProducer
import com.lightbend.lagom.scaladsl.persistence.PersistentEntityRegistry
import com.yuiwai.lagomraid.core.PlayerObject
import com.yuiwai.lagomraid.field.api.{FieldObject, FieldObjectTypes, FieldService, FindObjectsRequest}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class FieldServiceImpl(entityRegistry: PersistentEntityRegistry)
  (implicit ec: ExecutionContext)
  extends FieldService {
  // TOO 暫定的に単一ノードで全てのフィールドデータを扱う。スケールさせるには、エリア分割が必要
  private val fieldId = "field-id"
  def entity(id: String) = entityRegistry.refFor[FieldEntity](id)
  override def findObjects: ServiceCall[FindObjectsRequest, Seq[FieldObject]] = ServiceCall { request =>
    entity(fieldId)
      .ask(FindObjectsCommand(request.pos))
      .map(_.map {
        case PlayerObject(pos, playerId) =>
          api.FieldObject(pos, FieldObjectTypes.Player, playerId)
      })
  }
  override def putPlayer = ServiceCall { request =>
    entity(fieldId)
      .ask(PutPlayerCommand(request.playerId, MAX_POS))
      .flatMap {
        case Failure(e) => Future.failed(e)
        case Success(pos) => Future.successful(pos)
      }
  }
  override def playerTopic = {
    TopicProducer.taggedStreamWithOffset(FieldEvent.Tag) { (tag, offset) =>
      entityRegistry.eventStream(tag, offset)
        .collect {
          case ev if ev.event.isInstanceOf[FieldPlayerEvent] =>
            (convertPlayerEvent(ev.event.asInstanceOf[FieldPlayerEvent]), offset)
        }
    }
  }
  def convertPlayerEvent(event: FieldPlayerEvent) = event match {
    case PlayerPut(playerId, pos) => api.PlayerPutMessage(playerId, pos)
  }
}
