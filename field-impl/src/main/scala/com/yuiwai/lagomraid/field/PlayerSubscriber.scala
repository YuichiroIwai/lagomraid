package com.yuiwai.lagomraid.field

import akka.Done
import akka.stream.scaladsl.Flow
import com.lightbend.lagom.scaladsl.persistence.PersistentEntityRegistry
import com.yuiwai.lagomraid.player.api.{FieldMessage, PlayerService}

class PlayerSubscriber(entityRegistry: PersistentEntityRegistry, playerService: PlayerService) {
  playerService
    .fieldTopic
    .subscribe
    .atLeastOnce(Flow[FieldMessage].map { mesage =>
      Done
    })
}
