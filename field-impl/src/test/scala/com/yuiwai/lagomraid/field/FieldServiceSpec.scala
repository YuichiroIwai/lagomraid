package com.yuiwai.lagomraid.field

import com.lightbend.lagom.scaladsl.server.LocalServiceLocator
import com.lightbend.lagom.scaladsl.testkit.ServiceTest
import com.yuiwai.lagomraid.field.api.{FieldService, PutPlayerRequest}
import org.scalatest.{AsyncFlatSpec, Matchers}

class FieldServiceSpec extends AsyncFlatSpec with Matchers {
  lazy val server = ServiceTest.startServer(ServiceTest.defaultSetup.withJdbc()) { ctx =>
    new FieldApplication(ctx) with LocalServiceLocator
  }
  lazy val client = server.serviceClient.implement[FieldService]

  behavior of "A FieldService"

  it can "put new Player" in {
    client.putPlayer.invoke(PutPlayerRequest("player-id"))
      .map(_ should !==(""))
  }
}
