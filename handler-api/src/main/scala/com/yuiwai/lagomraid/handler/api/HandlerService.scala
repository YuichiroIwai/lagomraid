package com.yuiwai.lagomraid.handler.api

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import play.api.libs.json.{Format, Json}

object HandlerService {
  val entityId = "handlerEntityId"
}
trait HandlerService extends Service {
  def start: ServiceCall[NotUsed, Done]
  def stats: ServiceCall[NotUsed, StatsResponse]
  override def descriptor = {
    import Service._

    named("handler")
      .withCalls(
        pathCall("/api/handler/start", start _),
        pathCall("/api/handler/stats", stats _)
      )
      .withAutoAcl(true)
  }
}

case class StatsResponse(playerCount: Int)
object StatsResponse {
  implicit val format: Format[StatsResponse] = Json.format
}
