package com.yuiwai.lagomraid.handler

import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.persistence.slick.SlickPersistenceComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.lightbend.lagom.scaladsl.server.{LagomApplication, LagomApplicationContext, LagomApplicationLoader}
import com.softwaremill.macwire._
import com.yuiwai.lagomraid.field.api.FieldService
import com.yuiwai.lagomraid.handler.api.HandlerService
import com.yuiwai.lagomraid.player.api.PlayerService
import play.api.db.HikariCPComponents
import play.api.libs.ws.ahc.AhcWSComponents

import scala.collection.immutable

abstract class HandlerApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with SlickPersistenceComponents
    with HikariCPComponents
    with AhcWSComponents {
  override def lagomServer = serverFor[HandlerService](wire[HandlerServiceImpl])
  override def jsonSerializerRegistry = HandlerSerializerRegistry
  val playerService = serviceClient.implement[PlayerService]
  val fieldService = serviceClient.implement[FieldService]
  val scheduler: HandlerScheduler = wire[HandlerScheduler]
  persistentEntityRegistry.register(wire[HandlerEntity])
}

class HandlerApplicationLoader extends LagomApplicationLoader {
  override def load(context: LagomApplicationContext) = new HandlerApplication(context) {
    override def serviceLocator = NoServiceLocator
  }
  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new HandlerApplication(context) with LagomDevModeComponents
}

object HandlerSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: immutable.Seq[JsonSerializer[_]] = immutable.Seq()
}
