package com.yuiwai.lagomraid.handler

import java.time.Instant

import akka.Done
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity.ReplyType
import com.yuiwai.lagomraid.core.Pos
import com.yuiwai.lagomraid.handler.api.StatsResponse

class HandlerEntity extends PersistentEntity {
  override type Command = HandlerCommand[_]
  override type Event = HandlerEvent
  override type State = HandlerState
  override def initialState = Inactive
  override def behavior = {
    case Inactive =>
      Actions().onCommand[StartCommand.type, Done] {
        case (StartCommand, ctx, _) =>
          ctx.thenPersist(Started(Instant.now().getEpochSecond))(_ => ctx.reply(Done))
      }.onEvent {
        case (Started(now), _) => Active(now, 0)
      }
    case _: Active =>
      Actions().onReadOnlyCommand[StatsCommand.type, StatsResponse] {
        case (StatsCommand, ctx, state: Active) =>
          ctx.reply(StatsResponse(state.playerCount))
      }.onCommand[PlayerPutCommand, Done] {
        case (PlayerPutCommand(pos), ctx, _) =>
          ctx.thenPersist(PlayerPut(pos))(_ => ctx.reply(Done))
      }.onEvent {
        case (PlayerPut(_), state: Active) => state.playerPut
      }
  }
}

sealed trait HandlerCommand[R] extends ReplyType[R]
case object StartCommand extends HandlerCommand[Done]
case object StatsCommand extends HandlerCommand[StatsResponse]
case class PlayerPutCommand(pos: Pos) extends HandlerCommand[Done]

sealed trait HandlerEvent
case class Started(now: Long) extends HandlerEvent
case class PlayerPut(pos: Pos) extends HandlerEvent

sealed trait HandlerState
case object Inactive extends HandlerState
case class Active(startedAt: Long, playerCount: Int) extends HandlerState {
  def playerPut: HandlerState = copy(playerCount = playerCount + 1)
}
