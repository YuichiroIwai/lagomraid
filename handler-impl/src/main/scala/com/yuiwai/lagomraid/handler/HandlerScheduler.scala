package com.yuiwai.lagomraid.handler

import akka.actor.ActorSystem
import akka.stream.Materializer
import akka.stream.scaladsl.Source
import com.lightbend.lagom.scaladsl.persistence.PersistentEntityRegistry
import com.typesafe.config.ConfigFactory
import com.yuiwai.lagomraid.field.api.{FieldService, PutPlayerRequest}
import com.yuiwai.lagomraid.handler.api.HandlerService
import com.yuiwai.lagomraid.player.api.{PlayerService, RegisterRequest}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class HandlerScheduler(
  entityRegistry: PersistentEntityRegistry,
  system: ActorSystem,
  playerService: PlayerService,
  fieldService: FieldService)
  (implicit ec: ExecutionContext, materializer: Materializer) {
  val config = ConfigFactory.load()
  def entity = entityRegistry.refFor[HandlerEntity](HandlerService.entityId)
  def start(): Unit = {
    system.scheduler.schedule(
      0.second,
      config.getDuration("app.player.creation.interval").toMillis.millis,
      () => putPlayers(config.getInt("app.player.creation.count")))
  }
  def putPlayers(count: Int): Unit = {
    Source(1 to count)
      .mapAsync(2)(_ => playerService.register.invoke(RegisterRequest("name")))
      .mapAsync(2)(playerId => fieldService.putPlayer.invoke(PutPlayerRequest(playerId)))
      .runForeach(pos => entity.ask(PlayerPutCommand(pos)))
  }
}
