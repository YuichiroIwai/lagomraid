package com.yuiwai.lagomraid.handler

import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.persistence.PersistentEntityRegistry
import com.yuiwai.lagomraid.handler.api.HandlerService

import scala.concurrent.ExecutionContext
import scala.util.Success

class HandlerServiceImpl(entityRegistry: PersistentEntityRegistry, scheduler: HandlerScheduler)
  (implicit ec: ExecutionContext)
  extends HandlerService {
  def entity = entityRegistry.refFor[HandlerEntity](HandlerService.entityId)
  override def start = ServiceCall { _ =>
    entity.ask(StartCommand)
      .andThen {
        case Success(_) =>
          scheduler.start()
      }
  }
  override def stats = ServiceCall { _ =>
    entity.ask(StatsCommand)
  }
}
