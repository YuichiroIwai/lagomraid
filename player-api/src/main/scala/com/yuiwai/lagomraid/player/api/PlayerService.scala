package com.yuiwai.lagomraid.player.api

import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import play.api.libs.json.{Format, Json}

object PlayerService {
  val SCHEDULE_TOPIC_NAME = "playerScheduleTopic"
  val FIELD_TOPIC_NAME = "playerFieldTopic"
}
trait PlayerService extends Service {
  def register: ServiceCall[RegisterRequest, String]
  def scheduleTopic: Topic[ScheduleMessage]
  def fieldTopic: Topic[FieldMessage]
  override def descriptor = {
    import Service._

    named("player")
      .withCalls(
        pathCall("/api/player", register _)
      )
      .withTopics(
        topic(PlayerService.SCHEDULE_TOPIC_NAME, scheduleTopic),
        topic(PlayerService.FIELD_TOPIC_NAME, fieldTopic)
      )
      .withAutoAcl(true)
  }
}

case class RegisterRequest(name: String)
object RegisterRequest {
  implicit val format: Format[RegisterRequest] = Json.format
}

case class ScheduleMessage(playerId: String, executionTime: Long)
object ScheduleMessage {
  implicit val format: Format[ScheduleMessage] = Json.format
}

case class FieldMessage(playerId: String)
object FieldMessage {
  implicit val format: Format[FieldMessage] = Json.format
}
