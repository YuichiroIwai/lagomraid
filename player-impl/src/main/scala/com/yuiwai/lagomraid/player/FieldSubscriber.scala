package com.yuiwai.lagomraid.player

import akka.Done
import akka.stream.scaladsl.Flow
import com.lightbend.lagom.scaladsl.persistence.PersistentEntityRegistry
import com.yuiwai.lagomraid.field.api.{FieldService, PlayerMessage, PlayerPutMessage}

class FieldSubscriber(val entityRegistry: PersistentEntityRegistry, val fieldService: FieldService) {
  fieldService
    .playerTopic
    .subscribe
    .atLeastOnce(Flow[PlayerMessage].map { message =>
      message match {
        case PlayerPutMessage(playerId, pos) =>
          entity(playerId)
            .ask(PutCommand(pos))
      }
      Done
    })
  protected def entity(playerId: String) = entityRegistry.refFor[PlayerEntity](playerId)
}
