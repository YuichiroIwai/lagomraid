package com.yuiwai.lagomraid.player

import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaComponents
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.persistence.slick.SlickPersistenceComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.lightbend.lagom.scaladsl.server.{LagomApplication, LagomApplicationContext, LagomApplicationLoader}
import com.softwaremill.macwire._
import com.yuiwai.lagomraid.field.api.FieldService
import com.yuiwai.lagomraid.player.api.PlayerService
import com.yuiwai.lagomraid.player.scheduler.api.PlayerSchedulerService
import play.api.db.HikariCPComponents
import play.api.libs.ws.ahc.AhcWSComponents

import scala.collection.immutable

abstract class PlayerApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with LagomKafkaComponents
    with SlickPersistenceComponents
    with HikariCPComponents
    with AhcWSComponents {
  override def lagomServer = serverFor[PlayerService](wire[PlayerServiceImpl])
  override def jsonSerializerRegistry: JsonSerializerRegistry = PlayerSerializerRegistry
  val schedulerService = serviceClient.implement[PlayerSchedulerService]
  val fieldService = serviceClient.implement[FieldService]
  persistentEntityRegistry.register(wire[PlayerEntity])
  wire[FieldSubscriber]
  wire[SchedulerSubscriber]
}

class PlayerApplicationLoader extends LagomApplicationLoader {
  override def load(context: LagomApplicationContext) = new PlayerApplication(context) {
    override def serviceLocator = NoServiceLocator
  }
  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new PlayerApplication(context) with LagomDevModeComponents
}

object PlayerSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: immutable.Seq[JsonSerializer[_]] = immutable.Seq()
}
