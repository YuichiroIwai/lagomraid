package com.yuiwai.lagomraid.player

import akka.Done
import com.lightbend.lagom.scaladsl.persistence.{AggregateEvent, AggregateEventTag, PersistentEntity}
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity.ReplyType
import com.yuiwai.lagomraid.core.Pos
import play.api.libs.json.{Format, Json}

class PlayerEntity extends PersistentEntity {
  override type Command = PlayerCommand[_]
  override type Event = PlayerEvent
  override type State = PlayerState
  val enemySearchTime = 10
  override def initialState: PlayerState = Inactive
  override def behavior: Behavior = {
    case Inactive =>
      Actions().onCommand[RegisterCommand, Done] {
        case (RegisterCommand(name), ctx, _) =>
          ctx.thenPersist(PlayerRegistered(name))(_ => ctx.reply(Done))
      }.onCommand[PutCommand, Done] {
        case (PutCommand(pos), ctx, _) =>
          ctx.thenPersist(PlayerPut(pos))(_ => ctx.reply(Done))
      }.onEvent {
        case (PlayerRegistered(_), state) => state
        case (PlayerPut(pos), _) => Active(pos, Waiting)
      }
    case Active(_, _) =>
      Actions().onCommand[SetTargetEnemyCommand, Done] {
        case (SetTargetEnemyCommand(targetPos), ctx, Active(pos, Searching)) =>
          // TODO ターゲット設定
          ctx.done
      }.onCommand[ScheduleCommand.type, Done] {
        case (ScheduleCommand, ctx, state) =>
          ctx.done
      }.onEvent {
        case (EnemySearched(_), state: Active) => state.copy(status = Searching)
      }
  }
}

sealed trait PlayerCommand[R] extends ReplyType[R]
case class RegisterCommand(name: String) extends PlayerCommand[Done]
object RegisterCommand {
  implicit val format: Format[RegisterCommand] = Json.format
}
case class PutCommand(pos: Pos) extends PlayerCommand[Done]
case class SetTargetEnemyCommand(pos: Pos) extends PlayerCommand[Done]
case object ScheduleCommand extends PlayerCommand[Done]

sealed trait PlayerEvent extends AggregateEvent[PlayerEvent] {
  override def aggregateTag = PlayerEvent.Tag
}
object PlayerEvent {
  val Tag = AggregateEventTag.sharded[PlayerEvent](32)
}
case class PlayerRegistered(name: String) extends PlayerEvent
case class PlayerPut(pos: Pos) extends PlayerEvent

sealed trait ScheduleEvent extends PlayerEvent {
  val executionTime: Long
}
sealed trait FieldEvent extends PlayerEvent

case class EnemySearched(executionTime: Long) extends ScheduleEvent

sealed trait PlayerState
case object Inactive extends PlayerState
case class Active(pos: Pos, status: PlayerStatus) extends PlayerState

sealed trait PlayerStatus
case object Waiting extends PlayerStatus
case object Searching extends PlayerStatus