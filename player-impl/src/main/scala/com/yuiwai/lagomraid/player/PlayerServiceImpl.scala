package com.yuiwai.lagomraid.player

import java.util.UUID

import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.broker.TopicProducer
import com.lightbend.lagom.scaladsl.persistence.PersistentEntityRegistry
import com.yuiwai.lagomraid.player.api.{FieldMessage, PlayerService, RegisterRequest, ScheduleMessage}

import scala.concurrent.ExecutionContext

class PlayerServiceImpl(entityRegistry: PersistentEntityRegistry)
  (implicit ec: ExecutionContext)
  extends PlayerService {
  def entity(id: String) = entityRegistry.refFor[PlayerEntity](id)
  override def register: ServiceCall[RegisterRequest, String] = ServiceCall { request =>
    val playerId = UUID.randomUUID().toString
    entity(playerId)
      .ask(RegisterCommand(request.name))
      .map(_ => playerId)
  }
  override def scheduleTopic: Topic[ScheduleMessage] = {
    TopicProducer.taggedStreamWithOffset(PlayerEvent.Tag) { (tag, offset) =>
      entityRegistry.eventStream(tag, offset)
        .filter(_.event.isInstanceOf[ScheduleEvent])
        .map { ev =>
          ev.event match {
            case event: ScheduleEvent =>
              (api.ScheduleMessage(ev.entityId, event.executionTime), ev.offset)
          }
        }
    }
  }
  override def fieldTopic: Topic[FieldMessage] = {
    TopicProducer.taggedStreamWithOffset(PlayerEvent.Tag) { (tag, offset) =>
      entityRegistry.eventStream(tag, offset)
        .collect {
          case ev if ev.event.isInstanceOf[FieldEvent] =>
            ev.event match {
              case event: FieldEvent =>
                (api.FieldMessage(ev.entityId), ev.offset)
            }
        }
    }
  }
}
