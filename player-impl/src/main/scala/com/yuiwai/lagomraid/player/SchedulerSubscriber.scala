package com.yuiwai.lagomraid.player

import akka.Done
import akka.stream.scaladsl.Flow
import com.lightbend.lagom.scaladsl.persistence.PersistentEntityRegistry
import com.yuiwai.lagomraid.player.scheduler.api.{PlayerSchedulerService, ScheduleMessage}

class SchedulerSubscriber(entityRegistry: PersistentEntityRegistry, schedulerService: PlayerSchedulerService) {
  schedulerService
    .scheduleTopic
    .subscribe
    .atLeastOnce(Flow[ScheduleMessage].map { message =>
      entityRegistry
        .refFor[PlayerEntity](message.playerId)
        .ask(ScheduleCommand)
      Done
    })
}
