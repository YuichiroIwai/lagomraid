package com.yuiwai.lagomraid.player

import com.lightbend.lagom.scaladsl.server.LocalServiceLocator
import com.lightbend.lagom.scaladsl.testkit.ServiceTest
import com.yuiwai.lagomraid.player.api.{PlayerService, RegisterRequest}
import org.scalatest.{AsyncFlatSpec, Matchers}

class PlayerServiceSpec extends AsyncFlatSpec with Matchers {
  lazy val server = ServiceTest.startServer(ServiceTest.defaultSetup.withJdbc()) { ctx =>
    new PlayerApplication(ctx) with LocalServiceLocator
  }
  lazy val client = server.serviceClient.implement[PlayerService]

  behavior of "A PlayerService"

  it can "register new Player" in {
    client.register.invoke(RegisterRequest("name"))
      .map(_ should !==(""))
  }
}
