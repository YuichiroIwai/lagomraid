package com.yuiwai.lagomraid.player.scheduler.api

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import play.api.libs.json.{Format, Json}

object PlayerSchedulerService {
  val TOPIC_NAME = "playerSchedulerTopic"
  val ENTITY_ID = "playerScheduleEntityId"
}
trait PlayerSchedulerService extends Service {
  def schedules: ServiceCall[NotUsed, Seq[PlayerSchedule]]
  def scheduleTopic: Topic[ScheduleMessage]
  override def descriptor = {
    import Service._

    named("playerScheduler")
      .withCalls(
        pathCall("/api/player/schedules", schedules _)
      )
      .withTopics(
        topic(PlayerSchedulerService.TOPIC_NAME, scheduleTopic)
      )
      .withAutoAcl(true)
  }
}

case class PlayerSchedule(playerId: String, executeAt: Long)
object PlayerSchedule {
  implicit val format: Format[PlayerSchedule] = Json.format
}

case class ScheduleMessage(playerId: String)
object ScheduleMessage {
  implicit val format: Format[ScheduleMessage] = Json.format
}
