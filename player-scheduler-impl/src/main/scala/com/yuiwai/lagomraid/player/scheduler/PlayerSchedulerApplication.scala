package com.yuiwai.lagomraid.player.scheduler

import akka.actor.ActorSystem
import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaComponents
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.persistence.PersistentEntityRegistry
import com.lightbend.lagom.scaladsl.persistence.slick.SlickPersistenceComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.lightbend.lagom.scaladsl.server.{LagomApplication, LagomApplicationContext, LagomApplicationLoader}
import com.softwaremill.macwire._
import com.yuiwai.lagomraid.player.api.PlayerService
import com.yuiwai.lagomraid.player.scheduler.api.PlayerSchedulerService
import play.api.db.HikariCPComponents
import play.api.libs.ws.ahc.AhcWSComponents

import scala.collection.immutable
import scala.concurrent.ExecutionContext

abstract class PlayerSchedulerApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with LagomKafkaComponents
    with SlickPersistenceComponents
    with HikariCPComponents
    with AhcWSComponents {
  override def lagomServer = serverFor[PlayerSchedulerService](wire[PlayerSchedulerServiceImpl])
  override def jsonSerializerRegistry = PlayerSchedulerSerializerRegistry
  val playerService = serviceClient.implement[PlayerService]
  persistentEntityRegistry.register(wire[PlayerSchedulerEntity])
  wire[PlayerSubscriber]
  wire[Ticker]
}
class PlayerSchedulerApplicationLoader extends LagomApplicationLoader {
  override def load(context: LagomApplicationContext) =
    new PlayerSchedulerApplication(context) {
      override def serviceLocator = NoServiceLocator
    }
  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new PlayerSchedulerApplication(context) with LagomDevModeComponents
}


object PlayerSchedulerSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: immutable.Seq[JsonSerializer[_]] = immutable.Seq()
}

class Ticker(entityRegistry: PersistentEntityRegistry, system: ActorSystem)
  (implicit ec: ExecutionContext) {
  import scala.concurrent.duration._
  system.scheduler.schedule(1.second, 1.second, () => {
    entityRegistry.refFor[PlayerSchedulerEntity](PlayerSchedulerService.ENTITY_ID)
      .ask(Tick)
  })
}