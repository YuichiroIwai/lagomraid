package com.yuiwai.lagomraid.player.scheduler

import java.time.Instant

import akka.Done
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity.ReplyType
import com.lightbend.lagom.scaladsl.persistence.{AggregateEvent, AggregateEventTag, AggregateEventTagger, PersistentEntity}

import scala.collection.SortedSet

class PlayerSchedulerEntity extends PersistentEntity {
  override type Command = PlayerSchedulerCommand[_]
  override type Event = PlayerSchedulerEvent
  override type State = PlayerSchedulerState
  override def initialState: PlayerSchedulerState = PlayerSchedulerState.empty
  override def behavior: Behavior = {
    Actions().onCommand[AddScheduleCommand, Done] {
      case (AddScheduleCommand(playerId, executionTime), ctx, _) =>
        ctx.thenPersist(ScheduleAdded(playerId, Instant.now().getEpochSecond + executionTime))(_ => ctx.reply(Done))
    }.onReadOnlyCommand[GetSchedules.type, Seq[PlayerSchedule]]{
      case (GetSchedules, ctx, state) => ctx.reply(state.queue.toSeq)
    }.onEvent {
      case (ScheduleAdded(playerId, executeAt), state) =>
        state.add(PlayerSchedule(playerId, executeAt))
    }
  }
}

sealed trait PlayerSchedulerCommand[R] extends ReplyType[R]
case class AddScheduleCommand(playerId: String, executionTime: Long) extends PlayerSchedulerCommand[Done]
case object Tick extends PlayerSchedulerCommand[Done]
case object GetSchedules extends PlayerSchedulerCommand[Seq[PlayerSchedule]]

sealed trait PlayerSchedulerEvent extends AggregateEvent[PlayerSchedulerEvent] {
  val playerId: String
  override def aggregateTag: AggregateEventTagger[PlayerSchedulerEvent] = PlayerSchedulerEvent.Tag
}
object PlayerSchedulerEvent {
  val Tag = AggregateEventTag[PlayerSchedulerEvent]
}
case class ScheduleAdded(playerId: String, executeAt: Long) extends PlayerSchedulerEvent

case class PlayerSchedule(playerId: String, executeAt: Long)
case class PlayerSchedulerState(private[lagomraid] val queue: SortedSet[PlayerSchedule]) {
  def head: PlayerSchedule = head(Instant.now().getEpochSecond)
  def head(now: Long): PlayerSchedule = queue.find(_.executeAt <= now).get
  def tail: PlayerSchedulerState = tail(Instant.now().getEpochSecond)
  def tail(now: Long): PlayerSchedulerState =
    if (hasNext(now)) copy(queue = queue.tail)
    else this
  def isEmpty: Boolean = queue.isEmpty
  def nonEmpty: Boolean = queue.nonEmpty
  def hasNext: Boolean = hasNext(Instant.now().getEpochSecond)
  def hasNext(now: Long): Boolean = queue.headOption.exists(_.executeAt <= now)
  def add(playerSchedule: PlayerSchedule): PlayerSchedulerState =
    copy(queue = queue + playerSchedule)
}
object PlayerSchedulerState {
  implicit object PlayerScheduleOrdering extends Ordering[PlayerSchedule] {
    override def compare(x: PlayerSchedule, y: PlayerSchedule): Int = x.executeAt.compareTo(y.executeAt)
  }
  def empty = PlayerSchedulerState(SortedSet.empty[PlayerSchedule])
}
