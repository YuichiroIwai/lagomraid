package com.yuiwai.lagomraid.player.scheduler

import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.broker.TopicProducer
import com.lightbend.lagom.scaladsl.persistence.{EventStreamElement, PersistentEntityRegistry}
import com.yuiwai.lagomraid.player.scheduler.api.{PlayerSchedulerService, ScheduleMessage}

import scala.concurrent.ExecutionContext

class PlayerSchedulerServiceImpl(entityRegistry: PersistentEntityRegistry)
  (implicit ec: ExecutionContext)
  extends PlayerSchedulerService {
  override def schedules = ServiceCall { _ =>
    entityRegistry
      .refFor[PlayerSchedulerEntity](PlayerSchedulerService.ENTITY_ID)
      .ask(GetSchedules)
      .map(_.map(ps => api.PlayerSchedule(ps.playerId, ps.executeAt)))
  }
  override def scheduleTopic: Topic[ScheduleMessage] =
    TopicProducer.singleStreamWithOffset { fromOffset =>
      entityRegistry.eventStream(PlayerSchedulerEvent.Tag, fromOffset)
        .map(ev => (convertEvent(ev), ev.offset))
    }
  def convertEvent(ev: EventStreamElement[PlayerSchedulerEvent]) = ev.event match {
    case _ => ScheduleMessage(ev.event.playerId)
  }
}
