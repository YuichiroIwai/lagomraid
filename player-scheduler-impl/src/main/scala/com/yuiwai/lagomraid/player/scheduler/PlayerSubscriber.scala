package com.yuiwai.lagomraid.player.scheduler

import akka.Done
import akka.stream.scaladsl.Flow
import com.lightbend.lagom.scaladsl.persistence.PersistentEntityRegistry
import com.yuiwai.lagomraid.player.api.{PlayerService, ScheduleMessage}

class PlayerSubscriber(entityRegistry: PersistentEntityRegistry, playerService: PlayerService) {
  playerService
    .scheduleTopic
    .subscribe
    .atLeastOnce(Flow[ScheduleMessage].map { message =>
      entityRegistry.refFor[PlayerSchedulerEntity](api.PlayerSchedulerService.ENTITY_ID)
        .ask(AddScheduleCommand(message.playerId, message.executionTime))
      Done
    })
}
