package com.yuiwai.lagomraid.player.scheduler

import org.scalatest.{FlatSpec, Matchers}

class PlayerSchedulerStateSpec extends FlatSpec with Matchers {
  private def added(executionTime: Long) = PlayerSchedulerState.empty.add(PlayerSchedule("", executionTime))
  private def added(executionTimes: Seq[Long]) = executionTimes.foldLeft(PlayerSchedulerState.empty) {
    case (acc, executionTime) => acc.add(PlayerSchedule("", executionTime))
  }

  behavior of "Empty PlayerSchedulerState"

  it should "not have any messages" in {
    PlayerSchedulerState.empty.hasNext shouldBe false
    PlayerSchedulerState.empty.isEmpty shouldBe true
  }

  behavior of "A PlayerSchedulerState"

  it can "add new message" in {
    PlayerSchedulerState.empty
      .add(PlayerSchedule("", 0))
      .nonEmpty shouldBe true
  }

  behavior of "A PlayerSchedulerState with non executable message"

  it should "not have next message" in {
    val time = 10
    added(time).hasNext(time - 1) shouldBe false
  }

  behavior of "A PlayerSchedulerState with non executable message"

  it should "have next message" in {
    val time = 10
    added(time).hasNext(time) shouldBe true
  }

  behavior of "A PlayerScheduler with multiple executable messages"

  it should "order by executionTime" in {
    val time = 10L
    val state = added(Seq(time, time + 1, time - 1))
    state.head.executeAt shouldBe (time - 1)
    state.tail.head.executeAt shouldBe time
    state.tail.tail.head.executeAt shouldBe (time + 1)
    state.tail.tail.tail.hasNext shouldBe false
    state.tail.tail.tail.isEmpty shouldBe true
  }
}
